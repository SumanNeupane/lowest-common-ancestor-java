# Lowest Common Ancestor in Java #


## Visiual representation of the tree ##
##     ##
         14           
        /  \              
      9    12               
     / \   / \               
    6   8 10  11                
       / \                
      4   1

** Expected results: **

#### Lowest Common Ancestor of 4 and 1 is 8 ####
#### Lowest Common Ancestor of 10 and 11 is 12 ####
#### Lowest Common Ancestor of 6 and 8 is 9 ####
#### Lowest Common Ancestor of 9 and 12 is 14 ####
