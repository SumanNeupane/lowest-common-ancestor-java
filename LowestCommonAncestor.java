import java.util.ArrayList;
import java.util.List;

class Node { 
    int data; 
    Node left, right; 
   
    Node(int item) { 
        data = item; 
        left = right = null; 
    } 
} 
 
public class LowestCommonAncestor  { 
    private List<Integer> path1 = new ArrayList<>(); 
    private List<Integer> path2 = new ArrayList<>();
	Node root; 
    public static void main(String args[]) {  
    	int x, y;
        LowestCommonAncestor tree = new LowestCommonAncestor();                                                      
        tree.root = new Node(14);                   //               14           
        tree.root.left = new Node(9);               //              /  \              
        tree.root.right = new Node(12);             //             9    12               
        tree.root.left.left = new Node(6);          //            / \   / \               
        tree.root.left.right = new Node(8);         //           6   8 10  11                
        tree.root.right.left = new Node(10);        //              / \                
        tree.root.right.right = new Node(11);       //             4   1                
        tree.root.left.right.left = new Node(4);                    
        tree.root.left.right.right = new Node(1);                   

        x = 4; y = 1;
        System.out.println("Lowest Common Ancestor of " + x +" and " + y + " is: " + tree.findLowestCommonAncestor(x,y)+"."); 
        x = 8; y = 6;
        System.out.println("Lowest Common Ancestor of " + x +" and " + y + " is: " + tree.findLowestCommonAncestor(x,y)+".");
        x= 1; y = 11;
        System.out.println("Lowest Common Ancestor of " + x +" and " + y + " is: " + tree.findLowestCommonAncestor(x,y)+"."); 
        x = 10; y = 11;
        System.out.println("Lowest Common Ancestor of " + x +" and " + y + " is: " + tree.findLowestCommonAncestor(x,y)+"."); 
    }  
  
    int findLowestCommonAncestor(int X, int Y) { 
        path1.clear(); 
        path2.clear(); 
        return lowestCommonAncestor(root, X, Y); 
    } 
  
    private int lowestCommonAncestor(Node root, int X, int Y) { 
        if (!findPath(root, X, path1) || !findPath(root, Y, path2)) {  
        	System.out.println("Missing number, check your input.");
            return -1; 
        } 
        int i;
        for (i = 0; i < path1.size() && i < path2.size(); i++) { 
              if (!path1.get(i).equals(path2.get(i)))
                break; 
        } 
        return path1.get(i-1); 
    } 
      
    private boolean findPath(Node root, int n, List<Integer> path) { 
        if (root == null) return false;
          
        path.add(root.data); 
        if (root.data == n) return true; 
        if (root.left != null && findPath(root.left, n, path)) return true; 
        if (root.right != null && findPath(root.right, n, path)) return true;
       
        path.remove(path.size()-1); 
        return false; 
    }
}